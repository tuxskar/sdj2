package test;

import utility.collection.LinkedList_Version2;

public class LinkedListTest {

    public static void printStringList(LinkedList_Version2<String> list) {
        int size = list.size();
        for (int i= 0; i < size; i++) {
            String value = list.get(i);
            System.out.printf("[%d] %s", i, value);
        }
    }

                                        
	public static void main(String[] args) {
	    LinkedList_Version2<String> list = new LinkedList_Version2<String>();
        boolean passes;

		// is empty when created
		System.out.print(">> The list is empty when first created:\t");
		passes = list.isEmpty();
		
	    if (passes) {
	        System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }

        // add a node to the beggining of the list
		System.out.print(">> Add a node in the beggining of the list:\t");
        list.add(0, "Alice");
		passes = !list.isEmpty() && list.indexOf("Alice") == 0 && list.size() == 1;
		
	    if (passes) {
	        System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        
        // add a node to the end of the list
		System.out.print(">> Add a node in the end of the list:\t");
        list.add(1, "Bob");
		passes = !list.isEmpty() && list.indexOf("Bob") == 1 && list.size() == 2;
		
		if (passes) {
			System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        
        // add a node in the middle of the list
		System.out.print(">> Add a node in the middle of the list:\t");
        list.add(1, "Herman");
		passes = !list.isEmpty() && list.indexOf("Herman") == 1 && list.size() == 3;
		
		if (passes) {
			System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }

        printStringList(list);

	}

}
