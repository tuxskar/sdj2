package utility.collection;

public class LinkedList_Version2<T> implements ListADT<T> {

	private int count;
	private LinearNode<T> front;

	public LinkedList_Version2() {
		count = 0;
		LinearNode<T> dummy = new LinearNode<T>();
		front = new LinearNode<T>();
		front.setNext(dummy);
	}

	@Override
	public void add(int index, T element) {
		// TODO Auto-generated method stub
		if (index == 0) {
			add(element);
		} else {
			if ((index >= 0) && (index <= count)) {
				LinearNode<T> previous = front.getNext();
				for (int i = -1; i < index - 1; i++) {
					previous = previous.getNext();
				}
				LinearNode<T> NewNode = new LinearNode<T>(element,
						previous.getNext());
				previous.setNext(NewNode);
				count++;
			}
		}
	}

	@Override
	public void add(T element) {
		// This insert the element in the first place of the list
		if (count == 0) {
			front.getNext().setNext(new LinearNode<T>(element));
			count++;
		} else {
			LinearNode<T> ptr = new LinearNode<T>(element, front.getNext()
					.getNext());
			front.getNext().setNext(ptr);
			count++;
		}
	}

	@Override
	public void set(int index, T element) {
		// TODO Auto-generated method stub
		if ((index >= 0) && (index <= count - 1)) {
			LinearNode<T> ptr = front.getNext();
			for (int i = -1; i < index; i++) {
				ptr = ptr.getNext();
			}
			ptr.setElement(element);
		}
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		if ((index >= 0) && (index <= count - 1)) {
			LinearNode<T> ptr = front.getNext();
			for (int i = -1; i < index; i++) {
				ptr = ptr.getNext();
			}
			return ptr.getElement();
		}
		return null;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		if ((index >= 0) && (index <= count - 1)) {
			LinearNode<T> previous = front.getNext();
			for (int i = -1; i < index - 1; i++) {
				previous = previous.getNext();
			}
			T value = previous.getNext().getElement();
			previous.setNext(previous.getNext().getNext());
			return value;
		}
		return null;
	}

	@Override
	public T remove(T element) {
		// TODO Auto-generated method stub
		if ((count > 0)) {
			LinearNode<T> previous = front.getNext();
			while ((previous.getNext() != null)
					&& (!previous.getNext().getElement().equals(element))) {
				previous = previous.getNext();
			}
			if (previous.getNext() == null) {
				return null;
			} else {
				T value = previous.getNext().getElement();
				previous.setNext(previous.getNext().getNext());
				return value;
			}
		}
		return null;
	}

	@Override
	public int indexOf(T element) {
		// TODO Auto-generated method stub
		if ((count > 0)) {
			int indexof = 0;
			LinearNode<T> ptr = front.getNext().getNext();
			while ((ptr != null) && (!ptr.getElement().equals(element))) {
				ptr = ptr.getNext();
				indexof++;
			}
			if (ptr == null) {
				return -1;
			} else {
				return indexof;
			}
		}
		return -1;
	}

	@Override
	public boolean contains(T element) {
		// TODO Auto-generated method stub-
		if ((count > 0)) {
			LinearNode<T> ptr = front.getNext().getNext();
			while ((ptr != null) && (!ptr.getElement().equals(element))) {
				ptr = ptr.getNext();
			}
			if (ptr == null) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return count == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return count;
	}

}
