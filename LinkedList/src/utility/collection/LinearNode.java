package utility.collection;

public class LinearNode<T> {
	private LinearNode<T> next;
	private T element;

	/**
	 * 
	 */
	public LinearNode() {
		this.next = null;
		this.element = null;
	}

	/**
	 * @param element
	 */
	public LinearNode(T element) {
		this.next = null; 
		this.element = element;
	}

	/**
	 * @param element
	 * @param next
	 */
	public LinearNode(T element, LinearNode<T> next) {
		this.next = next;
		this.element = element;
	}

	/**
	 * @return
	 */
	public LinearNode<T> getNext() {
		return  this.next;
	}

	/**
	 * @param node
	 */
	public void setNext(LinearNode<T> node) {
		this.next = node;
	}

	/**
	 * @return
	 */
	public T getElement() {
		return this.element;
	}

	/**
	 * @param elem
	 */
	public void setElement(T elem) {
		this.element = elem;
	}

}