/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.model;


import java.util.*;


public class MovieList
{
	private ArrayList<Movie> list = new ArrayList<Movie>();
	
	
	public void add( Movie movie )
	{
		list.add( movie );
	}
	
	
	public boolean exists( Movie movie )
	{
		return find( movie.getTitle() ) != null;
	}
	
	
	public int noMovies()
	{
		return list.size();
	}
	
	
	public Movie find( String title )
	{
		for( Movie m: list )
			if( title.equals( m.getTitle() ) )
				return m;
				
		return null;
	}
	
	
	public void delete( Movie movie )
	{
		for( int i = 0; i < list.size(); ++i )
			if( movie.getTitle().equals( list.get( i ).getTitle() ) ) {
				list.remove( i );
				
				return;
			}
	}
	
	
	public String toXml()
	{
		StringBuffer res = new StringBuffer();
		
		res.append( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" );
		res.append( "<movielist>" );
		
		for( Movie m: list )
			res.append( m.toXml() );
		
		res.append( "</movielist>" );
		
		return res.toString();
	}
}