/*
 * 11.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.model;


import dk.viauc.jpe.globals.xml.*;


public class Person
{
	private String job;
	private String name;
	
	
	public Person()
	{
		job = "";
		name = "";
	}
	
	
	public Person( String job, String name )
	{
		this.job = job;
		this.name = name;
	}
	
	
	public void setJob( String job )
	{
		this.job = job;
	}
	
	
	public void setName( String name )
	{
		this.name = name;
	}
	
	
	public String getJob()
	{
		return job;
	}
	
	
	public String getName()
	{
		return name;
	}
	
		
	public String toXml()
	{
		return "<person>" +
		       "<job>" + XmlUtil.stringToXml( job ) + "</job>" +
		       "<name>" + XmlUtil.stringToXml( name ) + "</name>" +
		       "</person>";
	}
}