/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.model;


import dk.viauc.jpe.globals.xml.*;

import java.util.*;


public class Movie
{
	private String title;
	private String year;
	private String country;
	private PersonList persons;
	
	
	public Movie()
	{
		title = "";
		year = "";
		country = "";
		persons = new PersonList();
	}
	
	
	public Movie( String title, String year, String country, PersonList persons )
	{
		this.title = title;
		this.year = year;
		this.country = country;
		this.persons = persons;
	}
	
	
	public Movie( Movie other )
	{
		this.title = other.title;
		this.year = other.year;
		this.country = other.country;
		this.persons = new PersonList( other.persons );
	}
	
	
	public void setTitle( String title )
	{
		this.title = title;
	}
	
	
	public void setYear( String year )
	{
		this.year = year;
	}
	
	
	public void setCountry( String country )
	{
		this.country = country;
	}
	
	
	public String getTitle()
	{
		return title;
	}
	
	
	public String getYear()
	{
		return year;
	}
	
	
	public String getCountry()
	{
		return country;
	}
	
	
	public int noPersons()
	{
		return persons.noPersons();
	}
	
	
	public Iterator<Person> getPersonIterator()
	{
		return persons.getIterator();
	}
	
	
	public void addPerson( Person person )
	{
		persons.add( person );
	}
	
	
	public void removePerson( Person person )
	{
		persons.remove( person );
	}
	
	
	public String toXml()
	{
		return "<movie>" +
		       "<title>" + XmlUtil.stringToXml( title ) + "</title>" +
		       "<year>" + XmlUtil.stringToXml( year ) + "</year>" +
		       "<country>" + XmlUtil.stringToXml( country ) + "</country>" +
		       persons.toXml() +
		       "</movie>";
	}
}