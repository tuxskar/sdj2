/*
 * 11.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.model;


import java.io.*;


public class MovieListWriter
	extends FileWriter
{
	public MovieListWriter( String fileName )
		throws IOException
	{
		super( fileName );
	}
	
	
	public void write( MovieList ml )
		throws IOException
	{
		write( ml.toXml() );
	}
}