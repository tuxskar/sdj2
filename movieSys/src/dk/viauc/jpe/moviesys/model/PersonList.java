/*
 * 11.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.model;


import java.util.*;


public class PersonList
{
	private ArrayList<Person> list;
	
	
	public PersonList()
	{
		list = new ArrayList<Person>();
	}
	
	
	public PersonList( PersonList other )
	{
		this.list = new ArrayList<Person>( other.list );
	}
	
	
	public void add( Person person )
	{
		list.add( person );
	}
	
	
	public void remove( Person person )
	{
		for( int i = 0; i < list.size(); ++i ) {
			Person p = list.get( i );
			
			if( p.getJob().equals( person.getJob() ) &&
			    p.getName().equals( person.getName() ) ) {
			    	
			    list.remove( i );
			    return;
			}
		}
	}
	
	
	public int noPersons()
	{
		return list.size();
	}
	
	
	public Iterator<Person> getIterator()
	{
		return list.iterator();
	}
	
	
	public String toXml()
	{
		StringBuffer res = new StringBuffer();
		
		for( Person p: list )
			res.append( p.toXml() );
		
		return res.toString();
	}
}