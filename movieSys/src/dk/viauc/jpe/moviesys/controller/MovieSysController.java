/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.controller;


import dk.viauc.jpe.moviesys.view.*;
import dk.viauc.jpe.moviesys.model.*;


import java.io.*;


public class MovieSysController
{
	private MovieSysView view;
	
	private String actualFileName = null;
	private MovieList actualMovieList = null;
	private Movie actualMovie = null;
	
	
	public void setView( MovieSysView view )
	{
		this.view = view;
		
		setNewState();
	}
	
	
	private void setNewState()
	{
		view.setState( actualFileName, actualMovieList, actualMovie );
	}
	
	
	public void newFile()
	{
		actualMovieList = new MovieList();
		actualFileName = null;
		actualMovie = null;
		
		setNewState();
	}
	
	
	public void openFile()
	{
		actualFileName = view.getOpenFileName();
		
		if( actualFileName != null ) {
			try {
				MovieListReader in = new MovieListReader( actualFileName );
				actualMovieList = in.read();
				actualMovie = null;
			} catch( Exception ex ) {
				ex.printStackTrace();
			
				view.error( "Trouble reading " + actualFileName );
			}
		}
		
		setNewState();
	}
	
	
	public void saveFile()
	{
		try {
			MovieListWriter out = new MovieListWriter( actualFileName );
			out.write( actualMovieList );
			out.close();
		} catch( IOException ex ) {
			ex.printStackTrace();
			
			view.error( "Trouble writing " + actualFileName );
		}
		
		setNewState();
	}
	
	
	public void saveFileAs()
	{
		actualFileName = view.getSaveFileName();
		
		if( actualFileName != null ) {
			if( !actualFileName.endsWith( ".xml" ) )
				actualFileName += ".xml";
				
			saveFile();
		}
	}
	
	
	public void closeFile()
	{
		// ??? could ask the user if he really means this
		
		actualMovieList = null;
		actualFileName = null;
		actualMovie = null;
		
		setNewState();
	}
	
	
	public void newMovie()
	{
		Movie movie = new Movie();
		
		if( view.inputMovie( movie ) ) {
			actualMovieList.add( movie );
			actualMovie = movie;
			
			setNewState();
		}
	}
	
	
	public void findMovie()
	{
		String title = view.inputMovieTitle();
		
		if( title != null ) {
			Movie movie = actualMovieList.find( title );
			
			if( movie == null )
				view.error( "Movie " + title + " not found" );
			else {
				actualMovie = movie;
				
				setNewState();
			}
		}
	}
	
	
	public void updateMovie()
	{
		Movie movie = new Movie( actualMovie );
		
		if( view.modifyMovie( movie ) ) {
			actualMovieList.delete( actualMovie );
			actualMovieList.add( movie );
			actualMovie = movie;
			
			setNewState();
		}
	}
	
	
	public void deleteMovie()
	{
		actualMovieList.delete( actualMovie );
		actualMovie = null;
		
		setNewState();
	}
	
	
	public void addPerson()
	{
		Person person = new Person();
		
		if( view.inputPerson( person ) ) {
			actualMovie.addPerson( person );
			
			setNewState();
		}
	}
	
	
	public void removePerson()
	{
		Person person = new Person();
		
		if( view.inputPerson( person ) ) {
			actualMovie.removePerson( person );
			
			setNewState();
		}
	}
	
	
	public String verifyMovieCreation( Movie movie )
	{
		String res = verifyMovieFields( movie );
		
		if( res != null )
			return res;
			
		if( actualMovieList.exists( movie ) )
			return "Movie already exists";
			
		return null;
	}
	
	
	public String verifyMovieModification( Movie movie )
	{
		String res = verifyMovieFields( movie );
		
		if( res != null )
			return res;
			
		if( !movie.getTitle().equals( actualMovie.getTitle() ) &&
		    actualMovieList.exists( movie ) )
			return "Movie already exists";
			
		return null;
	}
	
	
	private String verifyMovieFields( Movie movie )
	{
		if( movie.getTitle() == null || movie.getTitle().length() == 0 )
			return "Title must be non-empty";
			
		if( movie.getYear() == null || movie.getYear().length() == 0 )
			return "Year must be non-empty";
			
		if( !movie.getYear().matches( "\\d*" ) )
			return "Year must be numeric";
			
		return null;
	}
	
	
	public String verifyPerson( Person person )
	{
		if( person.getJob() == null || person.getJob().length() == 0 )
			return "Job must be non-empty";
			
		if( person.getName() == null || person.getName().length() == 0 )
			return "Name must be non-empty";
			
		return null;
	}
	
	
	public void exit()
	{
		// ??? could ask the user for permission to close
		
		System.exit( 0 );
	}
}