/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.view;


import dk.viauc.jpe.moviesys.controller.*;
import dk.viauc.jpe.moviesys.model.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class MovieDialog
	extends JDialog
{
	private static final int LABEL_WIDTH = 50;
	
	
	private MovieSysController controller;
	private Movie movie;
	private MovieSysFrame mainFrame;
	private boolean update;
	private boolean OK = false;
	
	
	private JTextField titleText = new JTextField( 40 );
	private JTextField yearText = new JTextField( 4 );
	private JTextField countryText = new JTextField( 40 );
	
	private JButton okButton = new JButton( "OK" );
	private JButton cancelButton = new JButton( "Cancel" );
	
	
	public MovieDialog( MovieSysFrame mainFrame, Movie movie, MovieSysController controller,
	                    boolean update )
	{
		super( mainFrame, "Movie", true );
		
		this.movie = movie;
		this.controller = controller;
		this.mainFrame = mainFrame;
		this.update = update;
		
		Box box = Box.createVerticalBox();
		
		box.add( createFieldPanel() );
		box.add( Box.createVerticalStrut( 20 ) );
		box.add( createButtonPanel() );
		
		add( box );

		pack();		
		setLocationRelativeTo( null );
		setVisible( true );
	}
	
	
	private Box createFieldPanel()
	{
		Box b = Box.createVerticalBox();
		
		b.add( createField( "Title", titleText, movie.getTitle() ) );
		b.add( createField( "Year", yearText, movie.getYear() ) );
		b.add( createField( "Country", countryText, movie.getCountry() ) );
		
		return b;
	}
	
	
	private JPanel createField( String label, JTextField field, String initialValue )
	{
		JPanel p = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		
		JLabel l = new JLabel( label );
		l.setPreferredSize( new Dimension( LABEL_WIDTH, (int) l.getPreferredSize().getHeight() ) );
		p.add( l );
		
		field.setText( initialValue );
		p.add( field );
		
		return p;
	}
	
	
	private JPanel createButtonPanel()
	{
		JPanel p = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		
		p.add( okButton );
		p.add( cancelButton );
		
		okButton.addActionListener( new OKButtonHandler() );
		cancelButton.addActionListener( new CancelButtonHandler() );
		
		return p;
	}
	
	
	public boolean isOK()
	{
		return OK;
	}
	
	
	private class OKButtonHandler
		implements ActionListener
	{
		// called when OK button is pressed
		
		public void actionPerformed( ActionEvent event )
		{
			movie.setTitle( titleText.getText() );
			movie.setYear( yearText.getText() );
			movie.setCountry( countryText.getText() );
			
			String message;
			
			if( update )
				message = controller.verifyMovieModification( movie ); // null means OK!
			else
				message = controller.verifyMovieCreation( movie ); // null means OK!
			
			if( message != null ) {
				mainFrame.error( message );
				
				return;
			}
			
			OK = true;
			
			dispose(); // closes down dialog
		}
	}
	
	
	private class CancelButtonHandler
		implements ActionListener
	{
		// called when cancel button is pressed
		
		public void actionPerformed( ActionEvent event )
		{
			OK = false;
			
			dispose(); // closes down dialog
		}
	}
}