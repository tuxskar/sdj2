/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.view;


import dk.viauc.jpe.moviesys.model.*;


public interface MovieSysView
{
	public void setState( String actualFileName, MovieList actualMovieList, Movie actualMovie );
	
	public String getSaveFileName();
	
	public String getOpenFileName();
	
	public boolean inputMovie( Movie movie );
	
	public boolean modifyMovie( Movie movie );
	
	public boolean inputPerson( Person person );
	
	public String inputMovieTitle();
	
	public void error( String text );
}