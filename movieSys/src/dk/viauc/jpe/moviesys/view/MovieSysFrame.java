/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys.view;


import dk.viauc.jpe.moviesys.controller.*;
import dk.viauc.jpe.moviesys.model.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


public class MovieSysFrame
	extends JFrame
	implements MovieSysView
{
	private static final String TITLE = "MovieSys";
	
	private MovieSysController controller;
	
	
	private JMenuItem newFileItem = new JMenuItem( "New" );
	private JMenuItem openFileItem = new JMenuItem( "Open" );
	private JMenuItem saveFileItem = new JMenuItem( "Save" );
	private JMenuItem saveFileAsItem = new JMenuItem( "Save As" );
	private JMenuItem closeFileItem = new JMenuItem( "Close" );
	private JMenuItem exitItem = new JMenuItem( "Exit" );
	
	private JMenuItem newMovieItem = new JMenuItem( "New" );
	private JMenuItem findMovieItem = new JMenuItem( "Find" );
	private JMenuItem updateMovieItem = new JMenuItem( "Update" );
	private JMenuItem deleteMovieItem = new JMenuItem( "Delete" );
	
	private JMenuItem addPersonItem = new JMenuItem( "Add" );
	private JMenuItem removePersonItem = new JMenuItem( "Remove" );
	
	private JTextArea displayArea = new JTextArea();
	
	
	public MovieSysFrame( MovieSysController controller )
	{
		super( TITLE );
		
		this.controller = controller;
		
		setJMenuBar( createMenuBar() );
		
		add( createDisplayArea() );
		
		setSize( 1000, 700 );
		setLocationRelativeTo( null );
		setExtendedState( MAXIMIZED_BOTH );
		setDefaultCloseOperation( DO_NOTHING_ON_CLOSE );
		
		addWindowListener( new FrameHandler() );
		
		setVisible( true );
	}
	
	
	// --- build the GUI ---
	
	
	private JMenuBar createMenuBar()
	{
		JMenuBar bar = new JMenuBar();
		
		bar.add( createFileMenu() );
		bar.add( createMovieMenu() );
		bar.add( createPersonMenu() );
		
		return bar;
	}
	
	
	private JMenu createFileMenu()
	{
		JMenu menu = new JMenu( "File" );
		
		menu.add( newFileItem );
		menu.add( openFileItem );
		menu.add( saveFileItem );
		menu.add( saveFileAsItem );
		menu.add( closeFileItem );
		menu.addSeparator();
		menu.add( exitItem );
		
		newFileItem.addActionListener( new NewFileItemHandler() );
		openFileItem.addActionListener( new OpenFileItemHandler() );
		saveFileItem.addActionListener( new SaveFileItemHandler() );
		saveFileAsItem.addActionListener( new SaveFileAsItemHandler() );
		closeFileItem.addActionListener( new CloseFileItemHandler() );
		exitItem.addActionListener( new ExitItemHandler() );
		
		return menu;
	}
	
	
	private JMenu createMovieMenu()
	{
		JMenu menu = new JMenu( "Movie" );
		
		menu.add( newMovieItem );
		menu.add( findMovieItem );
		menu.add( updateMovieItem );
		menu.add( deleteMovieItem );
		
		newMovieItem.addActionListener( new NewMovieItemHandler() );
		findMovieItem.addActionListener( new FindMovieItemHandler() );
		updateMovieItem.addActionListener( new UpdateMovieItemHandler() );
		deleteMovieItem.addActionListener( new DeleteMovieItemHandler() );
		
		return menu;
	}
	
	
	private JMenu createPersonMenu()
	{
		JMenu menu = new JMenu( "Person" );
		
		menu.add( addPersonItem );
		menu.add( removePersonItem );
		
		addPersonItem.addActionListener( new AddPersonItemHandler() );
		removePersonItem.addActionListener( new RemovePersonItemHandler() );
		
		return menu;
	}
	
	
	private JTextArea createDisplayArea()
	{
		displayArea.setEditable( false );
		displayArea.setFont( new Font( "Courier", Font.PLAIN, 24 ) );
		
		return displayArea;
	}
	
	
	// --- the methods of the MovieSysView interface ---
	
	
	public void setState( String actualFileName, MovieList actualMovieList, Movie actualMovie )
	{
		display( actualFileName, actualMovieList, actualMovie );
		
		newFileItem.setEnabled( true );
		openFileItem.setEnabled( true );
		saveFileItem.setEnabled( actualFileName != null );
		saveFileAsItem.setEnabled( actualMovieList != null );
		closeFileItem.setEnabled( actualMovieList != null );
		exitItem.setEnabled( true );
		
		newMovieItem.setEnabled( actualMovieList != null );
		findMovieItem.setEnabled( actualMovieList != null );
		updateMovieItem.setEnabled( actualMovie != null );
		deleteMovieItem.setEnabled( actualMovie != null );
		
		addPersonItem.setEnabled( actualMovie != null );
		removePersonItem.setEnabled( actualMovie != null && actualMovie.noPersons() > 0 );
	}
	
	
	private void display( String actualFileName, MovieList actualMovieList, Movie actualMovie )
	{
		displayArea.setText( "" );
		
		if( actualMovieList == null )
			displayArea.append( "File: [No file]\n" );
		else {
			if( actualFileName == null )
				displayArea.append( "File: [New File]\n\n" );
			else
				displayArea.append( "File: " + actualFileName + "\n\n" );
			
			displayArea.append( "" + actualMovieList.noMovies() + " movie(s)\n" );
				
			if( actualMovie != null ) {
				displayArea.append( "--------------------------------------------\n" );
				
				displayArea.append( "Actual Movie\n\n" );
				displayArea.append( "Title:   " + actualMovie.getTitle() + "\n" );
				displayArea.append( "Year:    " + actualMovie.getYear() + "\n" );
				displayArea.append( "Country: " + actualMovie.getCountry() + "\n" );
				
				for( Iterator<Person> i = actualMovie.getPersonIterator(); i.hasNext(); ) {
					Person p = i.next();
					displayArea.append( "Person:  " + p.getJob() + " " + p.getName() + "\n" );
				}
			}
		}
				
		displayArea.append( "--------------------------------------------\n" );
	}
	
	
	public String getSaveFileName()
	{
		JFileChooser fc = new JFileChooser();
		
		if( fc.showSaveDialog( this ) == JFileChooser.APPROVE_OPTION )
			return fc.getSelectedFile().getAbsolutePath();
		else
			return null;
	}
	
	
	public String getOpenFileName()
	{
		JFileChooser fc = new JFileChooser();
		
		if( fc.showOpenDialog( this ) == JFileChooser.APPROVE_OPTION )
			return fc.getSelectedFile().getAbsolutePath();
		else
			return null;
	}
	
	
	// returns true iff user presses OK and all fields are valid
	
	public boolean inputMovie( Movie movie )
	{
		MovieDialog dialog = new MovieDialog( this, movie, controller, false );
		
		return dialog.isOK();
	}
	
	
	// returns true iff user presses OK and all fields are valid
	
	public boolean modifyMovie( Movie movie )
	{
		MovieDialog dialog = new MovieDialog( this, movie, controller, true );
		
		return dialog.isOK();
	}
	
	
	// returns true iff user presses OK and all fields are valid
	
	public boolean inputPerson( Person person )
	{
		PersonDialog dialog = new PersonDialog( this, person, controller );
		
		return dialog.isOK();
	}
	
	
	public String inputMovieTitle()
	{
		return JOptionPane.showInputDialog( "Movie Title" );
	}
	
	
	public void error( String text )
	{
		JOptionPane.showMessageDialog( null, text, TITLE, JOptionPane.ERROR_MESSAGE );
	}
	
	
	// --- Event handlers - they simply call the controller ---
	
	
	private class FrameHandler
		extends WindowAdapter
	{
		// Called when the user presses the window closing (cross) button
		
		public void windowClosing( WindowEvent event )
		{
			controller.exit();
		}
	}
	
	
	private class NewFileItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.newFile();
		}
	}
	
	
	private class OpenFileItemHandler
		implements ActionListener
	{
		// Called when open file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.openFile();
		}
	}
	
	
	private class SaveFileItemHandler
		implements ActionListener
	{
		// Called when save file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.saveFile();
		}
	}
	
	
	private class SaveFileAsItemHandler
		implements ActionListener
	{
		// Called when save file as menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.saveFileAs();
		}
	}
	
	
	private class CloseFileItemHandler
		implements ActionListener
	{
		// Called when close file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.closeFile();
		}
	}
	
	
	private class ExitItemHandler
		implements ActionListener
	{
		// Called when exit menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.exit();
		}
	}
	
	
	private class NewMovieItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.newMovie();
		}
	}
	
	
	private class FindMovieItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.findMovie();
		}
	}
	
	
	private class UpdateMovieItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.updateMovie();
		}
	}
	
	
	private class DeleteMovieItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.deleteMovie();
		}
	}
	
	
	private class AddPersonItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.addPerson();
		}
	}
	
	
	private class RemovePersonItemHandler
		implements ActionListener
	{
		// Called when new file menu item is selected
		
		public void actionPerformed( ActionEvent event )
		{
			controller.removePerson();
		}
	}
}