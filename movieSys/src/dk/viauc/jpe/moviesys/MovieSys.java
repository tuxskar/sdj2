/*
 * 10.02.2011 Original version
 */
 
 
package dk.viauc.jpe.moviesys;


import dk.viauc.jpe.moviesys.controller.*;
import dk.viauc.jpe.moviesys.view.*;


public class MovieSys
{
	public static void main( String[] args )
	{
		MovieSysController controller = new MovieSysController();
		MovieSysView view = new MovieSysFrame( controller );
		
		controller.setView( view );
	}
}