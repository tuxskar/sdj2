/*
 * 14.02.2009 New package structure
 * ??.??.200? Original version
 */
 
package global.xml;


public class XmlUtil
{
	public static String stringToXml( String s )
	{
		String res = "";
		
		for( int i = 0; i < s.length(); ++i )
			switch( s.charAt(i) )
			{
				case '&': res += "&amp;"; break;
				case '<': res += "&lt;"; break;
				case '>': res += "&gt;"; break;
				
				default: res += s.charAt(i);
			}
			
		return res;
	}
	
}
