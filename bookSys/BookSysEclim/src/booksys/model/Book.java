
package booksys.model;

import global.xml.XmlUtil;

import java.util.Date;

public class Book {
	String title;
	String author;
	Date published;
	String edition;
	String language;
	
	public Book(String title, String author, Date published, String edition, String language){
		this.title = title;
		this.author = author;
		this.published = published;
		this.edition = edition;
		this.language = language;
	}

	public String getTitle(){
		return this.title;
	}

	public String getAuthor(){
		return this.author;
	}

	public String getEdition(){
		return this.edition;
	}

	public String getLanguage(){
		return this.language;
	
	}

	public void setEdition(String edition){
		this.edition = edition;	
	}

	public String toXml(){
		return "<book title=\""+XmlUtil.stringToXml(this.title)+">"
				+ "<published>" + XmlUtil.stringToXml(this.published.toString()) + "</published>"
				+ "<author>" + XmlUtil.stringToXml(this.author) + "</author>"
				+ "<edition>" + XmlUtil.stringToXml(this.edition) + "</edition>"
				+ "<language>" + XmlUtil.stringToXml(this.language) + "</language>"
				+ "</book>";
	}
}
